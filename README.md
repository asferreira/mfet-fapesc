1º Ir até a pasta que vocês querem que o projeto fique, abrir o terminal Git (clicar com o botão direito em algum lugar da pasta
e escolher a opção Git Bash here.

2º Após isso, entrar no site do bitbucket e clicar no nome do nosso projeto MFET FAPESC, na página que abrir
no canto superior direito tem um botãozinho escrito "Clone" cliquem nele, copiem o comando que aparece e colem  no terminal

Ex: git clone https://asferreira@bitbucket.org/asferreira/mfet-fapesc.git (o link de vocês será diferente).

O Comando git clone inicializa o repositório git na pasta e copia todos os arquivos que estão lá.

3º Após isso, abram o eclipse vão em File -> New Project  e na tela desmarquem a opção "Use default location", cliquem no botão
Browse e escolham a pasta do projeto que acabaram de baixar. Depois disso adicionem os jars necessários que estão na pasta
bibliotecas ao projeto e já podem rodar.º

4º Adicione os jars que estão na pasta bibliotecas ao projeto.

5º utilizem o comando git checkout -b nome_da_branch, para criar uma branch para que vocês possam programar sem 
alterar a nossa versão principal (que fica no branch master)

Após isso podem começar a programar normalmente

## COMANDOS ÚTEIS ##

git status | mostra uma visão geral de como a da branch atual está (arquivos novos, modificados e exlcuídos)

## RECEBENDO ARQUIVOS ##
Uma vez que vocês já tiverem feito o clone, para atualizar o projeto para a versão mais recente utiliza-se o comando

git pull origin nome_da_branch

Caso não exista nada novo, não ira baixar nada.

## ENVIANDO ARQUIVOS ##
Quando você cria ou modifica arquivos as mudanças não são enviadas, deve-se executa os seguintes comandos.

-- git add file ou git add  * --

O comando git add, adiciona arquivos para um commit. Ele adicionará apenas arquivos que forem modificados ou novos.

Digamos que você tenha criado duas novas classes, Classe1.java e Classe2.java e modificado Classe3.java

Você pode adicionar uma a uma utilizando: git add Classe1.java e ir mudando os nomes
 ou utilizar git add * para enviar todas as modificações (em arquivos, pastas e etc). Geralmente se utiliza git add *

-- git commit -m "mensagem"--
O comando commit, prepara um pacote de modificações para ser enviado. ele sempre tem o atributo -m "" e espera que se envie
uma mensagem junto, para explicar o que foi feito. Por exemplo digamos que você adicionou o método soma a Classe2.java,
um possível comando de commit (depois de ter feito o add) seria.

git commit -m "Adicionado o método soma a classe"

Caso não existam arquivos adicionados o comando git commit não faz nada.

-- git push origin nome_da_branch --

Este é o comando que realmente envia os commits para a sua branch, que depois será fundida (merge) com a versão principal.

## Removendo arquivos ##
As vezes você pode criar ou modificar arquivos e pastas sem querer e isto ira aparecer como mudanças no status.
para resolver casos assim existem dois comandos.

-- git rm arquivo --
Deleta o arquivo da pasta e do repositório.

Ex: git rm Classe2.java

-- git checkout arquivo --
Retira o arquivo do status de mudançaas.
Caso seja um arquivo que já exista descarta as modificaçõe feitas (o arquivo permanece na pasta e no git).

git checkout Classe2.java







