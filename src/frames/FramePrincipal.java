package frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

//JFreeChart pie
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

import negocios.Led;
import negocios.Projeto;

import org.jfree.chart.ChartPanel;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.chart.ChartFactory;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.category.CategoryDataset;
public class FramePrincipal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 8102967934665845638L;
	
	private JPanel pnPrincipal;
	private JMenuBar menuBar;
	private JMenu menuProjeto;
	private JMenuItem menuItemNovo;
	private JMenuItem menuItemAbrir;
	private JMenuItem menuItemExportar;
	private JMenuItem menuItemSalvar;
	private JMenuItem menuItemSalvarComo;
	private JMenuItem menuItemFechar;
	private ImageIcon imgIcn;
	private GridLayout gridLayout = new GridLayout(2,2);
	private DefaultPieDataset dfPieDataset = new DefaultPieDataset();
	private PieDataset pieDataset;
	private JFreeChart chart1, chart2, chart3;
	private ChartFactory cf;
	private PiePlot3D plot;
	private ChartPanel chartPanel1, chartPanel2, chartPanel3;
	private DefaultCategoryDataset dfCatDat;
	private CategoryDataset catDat;
	private Dimension tela;
	private ImageIcon fundoLed;
	private Graphics g;
	private BufferedImage image;
	// Objeto de negocio Projeto
	private Projeto projeto;
	
	
	public FramePrincipal(){
		/*MONTA A TELA*/
		this.projeto = new Projeto();
		//painel
		imgIcn = new ImageIcon("imagens/IF.png");
		this.setIconImage(imgIcn.getImage());
		
		
	
		//painel
		pnPrincipal = new JPanel();
		pnPrincipal.setPreferredSize(new Dimension(1366, 768));
		//frame
		//Encerra o programa ao fechar a tela
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Habilita o redimensionamento da tela
		setResizable( true );
		//Adiciona define o painel para a tela
		setContentPane(pnPrincipal);
		//Adicionando componentes no painel
		//barra de menuSistema
		menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		//menuSistema
		menuProjeto = new JMenu("Projeto");
		menuProjeto.setMnemonic(KeyEvent.VK_S);
		menuProjeto.getAccessibleContext().setAccessibleDescription("Projeto de Pesquisa IFSC");
		menuBar.add(menuProjeto);
		//item do menuSistema
		//novo
		menuItemNovo = new JMenuItem("Novo");
		menuItemNovo.setMnemonic(KeyEvent.VK_U);
		menuItemNovo.getAccessibleContext().setAccessibleDescription("Novo");
		menuItemNovo.addActionListener(this);
		menuProjeto.add(menuItemNovo);
		//abrir
		menuItemAbrir = new JMenuItem("Abrir");
		menuItemAbrir.setMnemonic(KeyEvent.VK_P);
		menuItemAbrir.getAccessibleContext().setAccessibleDescription("Abrir");
		menuItemAbrir.addActionListener(this);
		menuProjeto.add(menuItemAbrir);
		//salvar
		menuItemSalvar = new JMenuItem("Salvar");
		menuItemSalvar.setMnemonic(KeyEvent.VK_V);
		menuItemSalvar.getAccessibleContext().setAccessibleDescription("Salvar");
		menuItemSalvar.addActionListener(this);
		menuProjeto.add(menuItemSalvar);
		//salvar como
		menuItemSalvarComo = new JMenuItem("Salvar Como");
		menuItemSalvarComo.setMnemonic(KeyEvent.VK_V);
		menuItemSalvarComo.getAccessibleContext().setAccessibleDescription("Salvar Como");
		menuItemSalvarComo.addActionListener(this);
		menuProjeto.add(menuItemSalvarComo);
		//exportar
		menuItemExportar = new JMenuItem("Exportar");
		menuItemExportar.setMnemonic(KeyEvent.VK_V);
		menuItemExportar.getAccessibleContext().setAccessibleDescription("Exportar");
		menuItemExportar.addActionListener(this);
		menuProjeto.add(menuItemExportar);
		//fechar
		menuItemFechar = new JMenuItem("Fechar");
		menuItemFechar.setMnemonic(KeyEvent.VK_V);
		menuItemFechar.getAccessibleContext().setAccessibleDescription("Fechar");
		menuItemFechar.addActionListener(this);
		menuProjeto.add(menuItemFechar);
		/*menuItem = new JMenuItem("Teste com icone", new ImageIcon("images/middle.gif"));
		menuItem.setMnemonic(KeyEvent.VK_N);
		menuSistema.add(menuItem);
		//Separador
		menuSistema.addSeparator();
		//check box menuSistema item
		cbMenuItem = new JCheckBoxMenuItem("Ativar opcão X");
		cbMenuItem.setMnemonic(KeyEvent.VK_X);
		menuSistema.add(cbMenuItem);*/
		//menuSobre
		
		
	}
	
	public void ativaSimulacaoPrincipal(){
		System.out.println("a");
		pnPrincipal.setLayout(gridLayout);
		JPanel painelBotao = new JPanel(new BorderLayout());
		//p1.setBackground(new Color(12,34,165));
		
		fundoLed = new ImageIcon("imagens/fundoPainel.png");
		JLabel img = new JLabel("", fundoLed, JLabel.CENTER);
		JTextField txtLumens = new JTextField();
		txtLumens.setBounds(485, 95, 90, 15);
		txtLumens.setColumns(40);
		JTextField txtEficienciaDriver = new JTextField();
		txtEficienciaDriver.setBounds(485, 135, 90, 15);
		txtEficienciaDriver.setColumns(40);
		JTextField txtEficienciaDifusor = new JTextField();
		txtEficienciaDifusor.setBounds(485, 115, 90, 15);
		txtEficienciaDifusor.setColumns(40);
		JTextField txtVidaUtil = new JTextField();
		txtVidaUtil.setBounds(485, 155, 90, 15);
		txtVidaUtil.setColumns(40);
		JTextField txtEficacia = new JTextField();
		txtEficacia.setBounds(485, 75, 90, 15);
		txtEficacia.setColumns(40);
		JButton btCalcular = new JButton("Calcular");
		btCalcular.setBounds(435,192,90,25);
		painelBotao.add(txtEficacia);
		painelBotao.add(txtLumens);
		painelBotao.add(txtEficienciaDriver);
		painelBotao.add(txtEficienciaDifusor);
		painelBotao.add(txtVidaUtil);
		painelBotao.add(btCalcular);
		painelBotao.setBackground(new Color(255,255,255));
		painelBotao.add(img, BorderLayout.CENTER );		
		pnPrincipal.add(painelBotao);
		
		//Criando padr�o do gr�fico
				dfCatDat = new DefaultCategoryDataset();
		        dfCatDat.addValue(1, "Valores g(x)", "0");
		        dfCatDat.addValue(1.5, "Valores g(x)", "1");
		        dfCatDat.addValue(1.75, "Valores g(x)", "2");
		        dfCatDat.addValue(1.99, "Valores g(x)", "3");
		        catDat = dfCatDat;
		        chart1 = ChartFactory.createLineChart("Testando Line", "Eixo X", "Eixo Y", catDat, PlotOrientation.VERTICAL, true, true, true);
		        chartPanel1 = new ChartPanel(chart1);
		        chartPanel1.setPreferredSize(new java.awt.Dimension(580,300));
		
		JPanel p2 = new JPanel();
        p2.setBackground(new Color(255,255,255));
        // Gr�fico Pizza
        /*dfPieDataset.setValue("Teste 1", 50);
        dfPieDataset.setValue("Teste 2", 49);
        dfPieDataset.setValue("Teste 3", 1);
        pieDataset = dfPieDataset;
        chart = ChartFactory.createPieChart3D("Testando", pieDataset, true, true, false);
        plot = (PiePlot3D)chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(580,300));
        */
        p2.add(chartPanel1);
        pnPrincipal.add(p2);
        chart2 = ChartFactory.createLineChart("Testando Line", "Eixo X", "Eixo Y", catDat, PlotOrientation.VERTICAL, true, true, true);
        chartPanel2 = new ChartPanel(chart2);
        chartPanel2.setPreferredSize(new java.awt.Dimension(580,300));
        JPanel p3 = new JPanel();
        p3.setBackground(new Color(255,255,255));
        p3.add(chartPanel2);
        pnPrincipal.add(p3);
        chart3 = ChartFactory.createLineChart("Testando Line", "Eixo X", "Eixo Y", catDat, PlotOrientation.VERTICAL, true, true, true);
        chartPanel3 = new ChartPanel(chart3);
        chartPanel3.setPreferredSize(new java.awt.Dimension(580,300));
        JPanel p4 = new JPanel();
        p4.setBackground(new Color(255,255,255));
        p4.add(chartPanel3);
        pnPrincipal.add(p4);
        this.setContentPane(pnPrincipal); 
        
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(menuItemNovo)) {			
			pnPrincipal.removeAll();
			FrameNovo frame = new FrameNovo(this);
			frame.pack();
			frame.setTitle("Selecione o modelo do LED");
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
		}
		if (e.getSource().equals(menuItemAbrir)) {
			
		
			
		}
		if (e.getSource().equals(menuItemExportar)) {
			
			
		}
		if (e.getSource().equals(menuItemFechar)) {
			this.dispose();
			
		}


	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	

}
