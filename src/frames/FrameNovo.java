package frames;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import negocios.Led;
import negocios.LedJDBC;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;



public class FrameNovo extends JFrame implements ActionListener{
	//componentes da tela
	private JPanel pnNovo;
	private JLabel lbMarca;
	private JLabel lbCodComponente;
	private JLabel lbFluxoLumin;
	private JLabel lbTempCor;
	private JLabel lbIndReprod;
	private JTextField txMarca;
	private JTextField txCodComponente;
	private JTextField txFluxoLumin;
	private JTextField txTempCor;
	private JTextField txIndReprod;
	private JCheckBox cbMarca;
	private JCheckBox cbCodComponente;
	private JCheckBox cbFluxoLumin;
	private JCheckBox cbTempCor;
	private JCheckBox cbIndReprod;
	private JButton btIncluir;
	private JButton btUtilModSelec;
	private JButton btPesquisar;
	private ImageIcon imgIcn;
	private ImageIcon imgBt;
	//tabela
	private String[] colunas = new String[]{"Marca","C�d. do Componente","Fluxo Luminoso", "Temp. da Cor", "�ndice de Reprod. das Cores"};
	private String[][] dados = new String[][] {};
	private JTable tabela = new JTable();
	private DefaultTableModel model = new DefaultTableModel(dados , colunas );
	private JScrollPane scroll = new JScrollPane();
	//
	private FramePrincipal fp = null;
	private LedJDBC ledBD;
	private ArrayList<Led> leds;


	private static final long serialVersionUID = -890024671131719752L;

	public FrameNovo(FramePrincipal fp) {
		this.fp = fp;
		/*MONTA A TELA*/
		//painel
		imgIcn = new ImageIcon("imagens/IF.png");
		this.setIconImage(imgIcn.getImage());
		pnNovo = new JPanel();
		pnNovo.setPreferredSize(new Dimension(683, 384));
		pnNovo.setLayout(null);
		//frame
		//Desaloca o frame da memoria no fechamento
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		//Habilita o redimensionamento da tela
		setResizable( false );
		//Adiciona define o painel para a tela
		setContentPane(pnNovo);
		//Adicionando componentes no painel
		//labels
		lbMarca = new JLabel("Marca");
		//(int x, int y, int width, int height)
		lbMarca.setBounds(60, 15, 60, 15);
		pnNovo.add(lbMarca);
		lbCodComponente = new JLabel("C�digo do Componente");
		lbCodComponente.setBounds(60, 45, 200, 15);
		pnNovo.add(lbCodComponente);
		lbFluxoLumin = new JLabel("Fluxo Luminoso");
		lbFluxoLumin.setBounds(60, 75, 200, 15);
		pnNovo.add(lbFluxoLumin);
		lbTempCor = new JLabel("Temperatura da Cor");
		lbTempCor.setBounds(60, 105, 200, 15);
		pnNovo.add(lbTempCor);
		lbIndReprod = new JLabel("�ndice de Reprodu��o de Cores");
		lbIndReprod.setBounds(60, 135, 300, 15);
		pnNovo.add(lbIndReprod);
		//textFields
		txMarca = new JTextField();
		txMarca.setBounds(250, 10, 135, 30);
		txMarca.setColumns(10);
		pnNovo.add(txMarca);
		txMarca.setEnabled(false);
		txCodComponente = new JTextField();
		txCodComponente.setBounds(250, 40, 135, 30);
		txCodComponente.setColumns(40);
		pnNovo.add(txCodComponente);
		txCodComponente.setEnabled(false);
		txFluxoLumin = new JTextField();
		txFluxoLumin.setBounds(250, 70, 135, 30);
		txFluxoLumin.setColumns(40);
		pnNovo.add(txFluxoLumin);
		txFluxoLumin.setEnabled(false);
		txTempCor = new JTextField();
		txTempCor.setBounds(250, 100, 135, 30);
		txTempCor.setColumns(40);
		pnNovo.add(txTempCor);
		txTempCor.setEnabled(false);
		txIndReprod = new JTextField();
		txIndReprod.setBounds(250, 130, 135, 30);
		txIndReprod.setColumns(40);
		pnNovo.add(txIndReprod);
		txIndReprod.setEnabled(false);
		//checkbox
		cbMarca = new JCheckBox();
		cbMarca.setBounds(15, 7, 45, 30);
		pnNovo.add(cbMarca);
		cbMarca.addActionListener(this);
		cbCodComponente = new JCheckBox();
		cbCodComponente.setBounds(15, 37, 45, 30);
		pnNovo.add(cbCodComponente);
		cbCodComponente.addActionListener(this);
		cbFluxoLumin = new JCheckBox();
		cbFluxoLumin.setBounds(15, 67, 45, 30);
		pnNovo.add(cbFluxoLumin);
		cbFluxoLumin.addActionListener(this);
		cbTempCor = new JCheckBox();
		cbTempCor.setBounds(15, 97, 45, 30);
		pnNovo.add(cbTempCor);
		cbTempCor.addActionListener(this);
		cbIndReprod = new JCheckBox();
		cbIndReprod.setBounds(15, 127, 45, 30);
		pnNovo.add(cbIndReprod);
		cbIndReprod.addActionListener(this);
		//tabela

		tabela.setModel(model);
		scroll.setViewportView(tabela);
		scroll.setBounds(15, 170, 650, 150);
		pnNovo.add(scroll);
		tabela.setEnabled(true);
		//*this.model =  (DefaultTableModel) this.tabela.getModel();
		//inserindo dados na tabela
		ledBD = new LedJDBC();
		try{
			leds = ledBD.consultarLeds();
			for(Led led : leds){
				String codComponente = led.getCodigoLed();
				double fluxoLumin = led.getFluxoLuminoso();
				double indReprod = led.getIndiceReprod();
				double tempCor = led.getTemperaturaCor();
				String[] linha = new String[]{led.getMarcaLed(), codComponente, ""+fluxoLumin , ""+tempCor, ""+indReprod };
				model.addRow(linha);

			}

		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		//botao
		btIncluir = new JButton("Incluir novo LED");
		btIncluir.setBounds(30, 335, 150, 30);
		pnNovo.add(btIncluir);
		btUtilModSelec = new JButton("Utilizar modelo selecionado");
		btUtilModSelec.setBounds(190, 335, 190, 30);
		pnNovo.add(btUtilModSelec);
		imgBt = new ImageIcon("imagens/lupa.jpg.png");
		btPesquisar = new JButton("Pesquisar");
		btPesquisar.setBounds(450, 32, 100, 50);
		pnNovo.add(btPesquisar);
		//evento do botão
		btIncluir.addActionListener(this);
		btUtilModSelec.addActionListener(this);
		btPesquisar.addActionListener(this);

		/*ASSOCIA O BEAN COM A TELA*/
		/*if (usr != null) {
			//carregando dados iniciais para os campos
			this.usrBean = usr;
			//convertendo int to Integer
			Integer usrId = new Integer(this.usrBean.getId());
			//Convertendo Integer to String
			txMarca.setText (usrId.toString());
			// colocando campo Id em readonly
			txMarca.setEditable(false);
			txCodComponente.setText(this.usrBean.getNome());
			txFluxoLumin.setText(this.usrBean.getEmail());
		} else {
			//Instancia o bean se o parametro veio nulo
			this.usrBean = new UsuarioBean();
		}
		 */	
	}
	//Método para tratar eventos do frame

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(btIncluir)) {
			FrameCadLed frame = new FrameCadLed(tabela, model, pnNovo);
			frame.pack();
			frame.setTitle("Inclus�o de Novo Led");
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
		}
		if (e.getSource().equals(btUtilModSelec)) {
			//pega linha selecionada do JTable
			/*int linha = this.tabela.getSelectedRow();
			//captura dados a linha selecionada do JTable
			if( linha != -1){
				Led led = new Led();
				led.setMarcaLed((String)tabela.getValueAt(linha , 0));
				led.setCodigoLed((String)tabela.getValueAt(linha , 1));
				led.setFluxoLuminoso((double)tabela.getValueAt(linha , 2));
				//setar outros campos
				this.fp.getProjeto().setLed(led);
				this.fp.ativaSimulacaoPrincipal();
				this.dispose();

			}*/
			this.fp.ativaSimulacaoPrincipal();
			this.dispose();
			if (e.getSource().equals(btPesquisar)) {


			}
			if (e.getSource().equals(cbMarca)) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if (cb.isSelected())
				{
					txMarca.setEnabled(true);
				}
				else
				{
					txMarca.setEnabled(false);
					txMarca.setText("");
				}
			}

			if (e.getSource().equals(cbCodComponente)) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if (cb.isSelected())
				{
					txCodComponente.setEnabled(true);
				}
				else
				{
					txCodComponente.setEnabled(false);
					txCodComponente.setText("");
				}

			}

			if (e.getSource().equals(cbFluxoLumin)) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if (cb.isSelected())
				{
					txFluxoLumin.setEnabled(true);
				}
				else
				{
					txFluxoLumin.setEnabled(false);
					txFluxoLumin.setText("");
				}
			}

			if (e.getSource().equals(cbTempCor)) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if (cb.isSelected())
				{
					txTempCor.setEnabled(true);
				}
				else
				{
					txTempCor.setEnabled(false);
					txTempCor.setText("");
				}
			}
			if (e.getSource().equals(cbIndReprod)) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if (cb.isSelected())
				{
					txIndReprod.setEnabled(true);
				}
				else
				{
					txIndReprod.setEnabled(false);
					txIndReprod.setText("");
				}
			}

		}
	}
}
