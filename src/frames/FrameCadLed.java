package frames;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import negocios.Led;
import negocios.LedJDBC;

public class FrameCadLed extends JFrame implements ActionListener{
	//componentes da tela
	private JPanel painel;
	private JLabel lbMarca;
	private JLabel lbCodComponente;
	private JLabel lbFluxoLumin;
	private JLabel lbTempCor;
	private JLabel lbIndReprod;
	private JLabel lbV0Led;
	private JLabel lbV1Led;
	private JLabel lbV2Led;
	private JLabel lbV3Led;
	private JLabel lbD0Led;
	private JLabel lbD1Led;
	private JLabel lbD2Led;
	private JLabel lbD3Led;
	private JLabel lbLedKe;
	private JLabel lbPhi0;
	private JLabel lbLedKv;
	private JLabel lbLedT0;
	private JLabel lbLedRjc;
	private JLabel lbVidaUtil;
	private JTextField txMarca;
	private JTextField txCodComponente;
	private JTextField txFluxoLumin;
	private JTextField txTempCor;
	private JTextField txIndReprod;
	private JTextField txV0Led;
	private JTextField txV1Led;
	private JTextField txV2Led;
	private JTextField txV3Led;
	private JTextField txD0Led;
	private JTextField txD1Led;
	private JTextField txD2Led;
	private JTextField txD3Led;
	private JTextField txLedKe;
	private JTextField txPhi0;
	private JTextField txLedKv;
	private JTextField txLedT0;
	private JTextField txLedRjc;
	private JTextField txVidaUtil;
	private JButton btAdicionar;
	private JTable tabela;
	private DefaultTableModel model;
	private ImageIcon imgIcn;
	private Led led;
	private LedJDBC ledBD;
	private boolean deuCerto;
	private JPanel Pnovo;
	//private ArrayList<LedBean> leds;
	

	public FrameCadLed(JTable tabela, DefaultTableModel model, JPanel novo) {
		/*MONTA A TELA*/
		imgIcn = new ImageIcon("imagens/IF.png");
		this.setIconImage(imgIcn.getImage());
		
		this.tabela = tabela;
		this.model = model;
		this.Pnovo = novo;
		//painel
		painel = new JPanel();
		painel.setPreferredSize(new Dimension(600, 625));
		painel.setLayout(null);;
		//frame
		//Desaloca o frame da memoria no fechamento
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		//Desabilita o redimensionamento da tela
		this.setResizable( false );
		//Adiciona define o painel para a tela
		this.setContentPane(painel);
		//Adicionando componentes no painel
		lbMarca = new JLabel("Marca");
		//(int x, int y, int width, int height)
		lbMarca.setBounds(15, 15, 60, 15);
		painel.add(lbMarca);
		lbCodComponente = new JLabel("C�digo do Componente");
		lbCodComponente.setBounds(15, 45, 200, 15);
		painel.add(lbCodComponente);
		lbFluxoLumin = new JLabel("Fluxo Luminoso");
		lbFluxoLumin.setBounds(15, 75, 200, 15);
		painel.add(lbFluxoLumin);
		lbTempCor = new JLabel("Temperatura da Cor");
		lbTempCor.setBounds(15, 105, 200, 15);
		painel.add(lbTempCor);
		lbIndReprod = new JLabel("�ndice de Reprodu��o de Cores");
		lbIndReprod.setBounds(15, 135, 300, 15);
		painel.add(lbIndReprod);
		lbV0Led = new JLabel("V0");
		lbV0Led.setBounds(15, 165, 50, 15);
		painel.add(lbV0Led);
		lbV1Led = new JLabel("V1");
		lbV1Led.setBounds(115, 165, 50, 15);
		painel.add(lbV1Led);
		lbV2Led = new JLabel("V2");
		lbV2Led.setBounds(215, 165, 50, 15);
		painel.add(lbV2Led);
		lbV3Led = new JLabel("V3");
		lbV3Led.setBounds(315, 165, 50, 15);
		painel.add(lbV3Led);
		lbD0Led = new JLabel("D0");
		lbD0Led.setBounds(15, 285, 200, 15);
		painel.add(lbD0Led);
		lbD1Led = new JLabel("D1");
		lbD1Led.setBounds(15, 315, 200, 15);
		painel.add(lbD1Led);
		lbD2Led = new JLabel("D2");
		lbD2Led.setBounds(15, 345, 200, 15);
		painel.add(lbD2Led);
		lbD3Led = new JLabel("D3");
		lbD3Led.setBounds(15, 375, 200, 15);
		painel.add(lbD3Led);
		lbLedKe = new JLabel("Ke");
		lbLedKe.setBounds(15, 405, 200, 15);
		painel.add(lbLedKe);
		lbPhi0 = new JLabel("Phi 0");
		lbPhi0.setBounds(15, 435, 200, 15);
		painel.add(lbPhi0);
		lbLedKv = new JLabel("Kv");
		lbLedKv.setBounds(15, 465, 200, 15);
		painel.add(lbLedKv);
		lbLedT0 = new JLabel("T0");
		lbLedT0.setBounds(15, 495, 200, 15);
		painel.add(lbLedT0);
		lbLedRjc = new JLabel("Rjc");
		lbLedRjc.setBounds(15, 525, 200, 15);
		painel.add(lbLedRjc);
		lbVidaUtil = new JLabel("Vida Util");
		lbVidaUtil.setBounds(15, 555, 200, 15);
		painel.add(lbVidaUtil);
		//textFields
		txMarca = new JTextField();
		txMarca.setBounds(205, 10, 135, 30);
		txMarca.setColumns(10);
		painel.add(txMarca);
		txCodComponente = new JTextField();
		txCodComponente.setBounds(205, 40, 135, 30);
		txCodComponente.setColumns(40);
		painel.add(txCodComponente);
		txFluxoLumin = new JTextField();
		txFluxoLumin.setBounds(205, 70, 135, 30);
		txFluxoLumin.setColumns(40);
		painel.add(txFluxoLumin);
		txTempCor = new JTextField();
		txTempCor.setBounds(205, 100, 135, 30);
		txTempCor.setColumns(40);
		painel.add(txTempCor);
		txIndReprod = new JTextField();
		txIndReprod.setBounds(205, 130, 135, 30);
		txIndReprod.setColumns(40);
		painel.add(txIndReprod);
		txV0Led = new JTextField();
		txV0Led.setBounds(45, 160, 60, 30);
		painel.add(txV0Led);
		txV1Led = new JTextField();
		txV1Led.setBounds(145, 160, 60, 30);
		painel.add(txV1Led);
		txV2Led = new JTextField();
		txV2Led.setBounds(205, 220, 135, 30);
		painel.add(txV2Led);
		txV3Led = new JTextField();
		txV3Led.setBounds(205, 250, 135, 30);
		painel.add(txV3Led);
		txD0Led = new JTextField();
		txD0Led.setBounds(205, 280, 135, 30);
		painel.add(txD0Led);
		txD1Led = new JTextField();
		txD1Led.setBounds(205, 310, 135, 30);
		painel.add(txD1Led);
		txD2Led = new JTextField();
		txD2Led.setBounds(205, 340, 135, 30);
		painel.add(txD2Led);
		txD3Led = new JTextField();
		txD3Led.setBounds(205, 370, 135, 30);
		painel.add(txD3Led);
		txLedKe = new JTextField();
		txLedKe.setBounds(205, 400, 135, 30);
		painel.add(txLedKe);
		txPhi0 = new JTextField();
		txPhi0.setBounds(205, 430, 135, 30);
		painel.add(txPhi0);
		txLedKv = new JTextField();
		txLedKv.setBounds(205, 460, 135, 30);
		painel.add(txLedKv);
		txLedT0 = new JTextField();
		txLedT0.setBounds(205, 490, 135, 30);
		painel.add(txLedT0);
		txLedRjc = new JTextField();
		txLedRjc.setBounds(205, 520, 135, 30);
		painel.add(txLedRjc);
		txVidaUtil = new JTextField();
		txVidaUtil.setBounds(205, 550, 135, 30);
		painel.add(txVidaUtil);
		//botao
		btAdicionar = new JButton("Adicionar");
		btAdicionar.setBounds(15, 590, 120, 30);
		painel.add(btAdicionar);
		
		
		//evento do botão
		btAdicionar.addActionListener(this);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(btAdicionar)) {
			model =  (DefaultTableModel) tabela.getModel();
			String marca = txMarca.getText();
			String codComponente = txCodComponente.getText();
			double fluxoLumin = Double.parseDouble(txFluxoLumin.getText());
			double indReprod = Double.parseDouble(txIndReprod.getText());
			double tempCor = Double.parseDouble(txTempCor.getText());
			double v0 = Double.parseDouble(txV0Led.getText());
			double v1 = Double.parseDouble(txV1Led.getText());
			double v2 = Double.parseDouble(txV2Led.getText());
			double v3 = Double.parseDouble(txV3Led.getText());
			double d0 = Double.parseDouble(txD0Led.getText());
			double d1 = Double.parseDouble(txD1Led.getText());
			double d2 = Double.parseDouble(txD2Led.getText());
			double d3 = Double.parseDouble(txD3Led.getText());
			double ke = Double.parseDouble(txLedKe.getText());
			double phi0 = Double.parseDouble(txPhi0.getText());
			double kv = Double.parseDouble(txLedKv.getText());
			double t0 = Double.parseDouble(txLedT0.getText());
			double rjc = Double.parseDouble(txLedRjc.getText());
			String vidaUtil = txVidaUtil.getText();
			led = new Led();
			led.setMarcaLed(marca);
			led.setCodigoLed(codComponente);
			led.setFluxoLuminoso(fluxoLumin);
			led.setIndiceReprod(indReprod);
			led.setTemperaturaCor(tempCor);
			led.setV0Led(v0);
			led.setV1Led(v1);
			led.setV2Led(v2);
			led.setV3Led(v3);
			led.setD0Led(d0);
			led.setD1Led(d1);
			led.setD2Led(d2);
			led.setD3Led(d3);
			led.setLedKe(ke);
			led.setPhi0(phi0);
			led.setLedKv(kv);
			led.setLedT0(t0);
			led.setLedRjc(rjc);
			led.setVidaUtil(vidaUtil);
			
			
			
			
			String[] linha = new String[]{marca, codComponente, ""+fluxoLumin , ""+tempCor, ""+indReprod };
			if(marca.isEmpty() == true || codComponente.isEmpty() == true || txFluxoLumin.getText().isEmpty() == true  || txIndReprod.getText().isEmpty() == true || txTempCor.getText().isEmpty() == true || txV0Led.getText().isEmpty() == true || txV1Led.getText().isEmpty() == true || txV2Led.getText().isEmpty() == true || txV3Led.getText().isEmpty() == true || txD0Led.getText().isEmpty() == true || txD1Led.getText().isEmpty() == true || txD2Led.getText().isEmpty() == true || txD3Led.getText().isEmpty() == true || txLedKe.getText().isEmpty() == true || txPhi0.getText().isEmpty() == true || txLedKv.getText().isEmpty() == true || txLedT0.getText().isEmpty() == true || txLedRjc.getText().isEmpty() == true || txVidaUtil.getText().isEmpty() == true)
			{
				JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
			}
			
			else
			{
				try{
					ledBD = new LedJDBC();
					deuCerto = ledBD.inserirLed(led);
					if(deuCerto == false){
						JOptionPane.showMessageDialog(null, "N�o foi poss�vel inserir este modelo!");
					}
					else{
						this.dispose();
						model.addRow(linha);
						Pnovo.repaint();
						
						
					}
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
				
				//model.addRow(linha);
				txMarca.setText("");
				txCodComponente.setText("");
				txFluxoLumin.setText("");
				txIndReprod.setText("");
				txTempCor.setText("");
				txV0Led.setText("");
				txV1Led.setText("");
				txV2Led.setText("");
				txV3Led.setText("");
				txD0Led.setText("");
				txD1Led.setText("");
				txD2Led.setText("");
				txD3Led.setText("");
				txLedKe.setText("");
				txPhi0.setText("");
				txLedKv.setText("");
				txLedT0.setText("");
				txLedRjc.setText("");
				txVidaUtil.setText("");
			}
	
		}

	}



}
