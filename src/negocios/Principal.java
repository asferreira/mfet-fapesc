package negocios;

import javax.swing.JFrame;
import frames.FramePrincipal;

public class Principal {
	public static void main(String[] args) {
		FramePrincipal frame = new FramePrincipal();
		frame.pack();
		frame.setTitle("Projeto Teste");
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setVisible(true);
	}
}	
