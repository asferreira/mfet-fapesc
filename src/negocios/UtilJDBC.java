package negocios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class UtilJDBC {
	static final  String url  = "jdbc:postgresql://127.0.0.1:5432/db_mfetFapesc";
	static final  String usuario  =  "postgres";
	static final  String senha  =  "1234";


public static  Connection  getConnection() throws  Exception{
	Connection  con  =  null;
	try  {  
		Class.forName("org.postgresql.Driver");
		System.out.println("JDBC  Driver  Ok");
		con  =  DriverManager.getConnection(url,usuario,senha);
		if  (con  !=  null)  {
			System.out.println("Conex�o  Ok");
			return con;
			}  
		else  {
			throw new  Exception("A  conex�o  n�o  foi  criada!");  
			}
		}  
	catch  (ClassNotFoundException  e)  {
		System.out.println("Driver  JDBC  inv�lido");
		e.printStackTrace();
		}  
	catch  (SQLException  e)  {
		System.out.println("Falha  na  conex�o");
		e.printStackTrace();
		}
	return con;
	}
}