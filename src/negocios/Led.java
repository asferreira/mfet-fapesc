package negocios;

public class Led {
	private String codigoLed;
	private String marcaLed;
	private double fluxoLuminoso;
	private double temperaturaCor;
	private double indiceReprod;
	private double v0Led;
	private double v1Led;
	private double v2Led;
	private double v3Led;
	private double d0Led;
	private double d1Led;
	private double d2Led;
	private double d3Led;
	private double ledKe;
	private double phi0;
	private double ledKv;
	private double ledT0;
	private double ledRjc;
	private String vidaUtil;
	
	public String getVidaUtil() {
		return vidaUtil;
	}

	public void setVidaUtil(String vidaUtil) {
		this.vidaUtil = vidaUtil;
	}

	public String getCodigoLed() {
		return codigoLed;
	}

	public void setCodigoLed(String codigoLed) {
		this.codigoLed = codigoLed;
	}

	public String getMarcaLed() {
		return marcaLed;
	}

	public void setMarcaLed(String marcaLed) {
		this.marcaLed = marcaLed;
	}

	public double getFluxoLuminoso() {
		return fluxoLuminoso;
	}

	public void setFluxoLuminoso(double fluxoLuminoso) {
		this.fluxoLuminoso = fluxoLuminoso;
	}

	public double getTemperaturaCor() {
		return temperaturaCor;
	}

	public void setTemperaturaCor(double temperaturaCor) {
		this.temperaturaCor = temperaturaCor;
	}

	public double getIndiceReprod() {
		return indiceReprod;
	}

	public void setIndiceReprod(double indiceReprod) {
		this.indiceReprod = indiceReprod;
	}

	public double getV0Led() {
		return v0Led;
	}

	public void setV0Led(double v0Led) {
		this.v0Led = v0Led;
	}

	public double getV1Led() {
		return v1Led;
	}

	public void setV1Led(double v1Led) {
		this.v1Led = v1Led;
	}

	public double getV2Led() {
		return v2Led;
	}

	public void setV2Led(double v2Led) {
		this.v2Led = v2Led;
	}

	public double getV3Led() {
		return v3Led;
	}

	public void setV3Led(double v3Led) {
		this.v3Led = v3Led;
	}

	public double getD0Led() {
		return d0Led;
	}

	public void setD0Led(double d0Led) {
		this.d0Led = d0Led;
	}

	public double getD1Led() {
		return d1Led;
	}

	public void setD1Led(double d1Led) {
		this.d1Led = d1Led;
	}

	public double getD2Led() {
		return d2Led;
	}

	public void setD2Led(double d2Led) {
		this.d2Led = d2Led;
	}

	public double getD3Led() {
		return d3Led;
	}

	public void setD3Led(double d3Led) {
		this.d3Led = d3Led;
	}

	public double getLedKe() {
		return ledKe;
	}

	public void setLedKe(double ledKe) {
		this.ledKe = ledKe;
	}

	public double getPhi0() {
		return phi0;
	}

	public void setPhi0(double phi0) {
		this.phi0 = phi0;
	}

	public double getLedKv() {
		return ledKv;
	}

	public void setLedKv(double ledKv) {
		this.ledKv = ledKv;
	}

	public double getLedT0() {
		return ledT0;
	}

	public void setLedT0(double ledT0) {
		this.ledT0 = ledT0;
	}

	public double getLedRjc() {
		return ledRjc;
	}

	public void setLedRjc(double ledRjc) {
		this.ledRjc = ledRjc;
	}


	
	public Led(){
		super();
	}
	
	
	
	
}