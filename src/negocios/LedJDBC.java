package negocios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
public class LedJDBC {
	
	/*public void deletarLed(String cod) throws Exception{
		Statement  stmt  =  null;
		Connection  con  =  null;
		String  query = "DELETE from leds where codigoLed = '" +cod +"'";
		try  {
			con  =  UtilJDBC.getConnection();
			stmt  =  con.createStatement();
			stmt.executeUpdate(query);
		}  catch  (Exception  e  )  {
			System.out.println(e);
		}  finally  {
			if  (stmt  !=  null)  {  
				stmt.close();  
			}  
			if  (con  !=  null)  {  
				con.close();
			}
		}  
	}
	
	public boolean alterarLed(String cod, String marca, String temp, String ind, String fluxo) throws Exception{
		boolean retorno = false;
		int aux = 0;
		Statement  stmt  =  null;
		Connection  con  =  null;
		StringBuilder  queryBd  = new StringBuilder();
		queryBd.append("UPDATE  leds  SET ");
	    if(marca.isEmpty() == false)
	    {
	    	queryBd.append("marcaLed = '"+marca +"'");
	    	aux++;
	    }
	    if(temp.isEmpty() == false)
	    {
	    	if(aux != 0)
	    	{
	    		queryBd.append(", temperaturCor = '"+temp +"'");
	    	}
	    	
	    	else
	    	{
	    		queryBd.append("temperaturaCor = '"+temp +"'");
	    	}
	    	aux++;
	    }
	    if(fluxo.isEmpty() == false)
	    {
	    	if(aux != 0)
	    	{
	    		queryBd.append(", fluxoLuminoso = '" +fluxo +"'");
	    	}
	    	
	    	else
	    	{
	    		queryBd.append("fluxoLuminoso = '" +fluxo +"'");
	    	}
	    	aux++;
	    }
	    if(ind.isEmpty() == false)
	    {
	    	if(aux != 0)
	    	{
	    		queryBd.append(", ind_reprod = '" +ind +"'");
	    	}
	    	
	    	else
	    	{
	    		queryBd.append("ind_reprod = '" +ind +"'");
	    	}
	    	aux++;
	    }
	    queryBd.append(" WHERE cod_comp = '" +cod +"'");
	    String query = queryBd.toString();
	    try  {
			con  =  UtilJDBC.getConnection();
			stmt  =  con.createStatement();
			stmt.executeUpdate(query);
			retorno  =  true;
		}  catch  (Exception  e  )  {
			System.out.println(e);
		}  finally  {
			if  (stmt  !=  null)  {  
				stmt.close();  
			}  
			if  (con  !=  null)  {  
				con.close();
			}
		}  
		
		
		return retorno;
	}
	
	public ArrayList<Led> consultarLedProxMarca(String marca) throws Exception {
		ArrayList<Led> leds = new ArrayList<Led>();
		Led led;
		Statement  stmt  =  null;
		Connection  con  =  null;
		String  query  =  "select  cod_comp, marca, fluxo_lumin, temp_cor, ind_reprod  from  leds where marca like '%"+marca +"%'";
		try  {
			con  =  UtilJDBC.getConnection();
			stmt  =  con.createStatement();
			ResultSet  rs  =  stmt.executeQuery(query);
			while  (rs.next())  {
				led  =  new  Led();
				led.setCodComponente(rs.getString("cod_comp"));  
				led.setMarca(rs.getString("marca"));
				led.setFluxoLumin(rs.getString("fluxo_lumin"));
				led.setTempCor(rs.getString("temp_cor"));
				led.setIndReprod(rs.getString("ind_reprod"));
				leds.add(led); 
				System.out.println(led.getMarca());
			}
		}
		catch  (Exception  e  )  {
			System.out.println(e);
		}  finally  {
			if  (stmt  !=  null)  {  
				stmt.close();  
			}
			if  (con  !=  null)  {  
				con.close();  
			}
		}
	
		return leds;
	}
	
	public ArrayList<Led> consultarLedProxFluxo(String fluxo) throws Exception {
		ArrayList<Led> leds = new ArrayList<Led>();
		Led led;
		Statement  stmt  =  null;
		Connection  con  =  null;
		String  query  =  "select  cod_comp, marca, fluxo_lumin, temp_cor, ind_reprod  from  leds where fluxo_lumin like '%"+fluxo +"%'";
		try  {
			con  =  UtilJDBC.getConnection();
			stmt  =  con.createStatement();
			ResultSet  rs  =  stmt.executeQuery(query);
			while  (rs.next())  {
				led  =  new  Led();
				led.setCodComponente(rs.getString("cod_comp"));  
				led.setMarca(rs.getString("marca"));
				led.setFluxoLumin(rs.getString("fluxo_lumin"));
				led.setTempCor(rs.getString("temp_cor"));
				led.setIndReprod(rs.getString("ind_reprod"));
				leds.add(led); 
				System.out.println(led.getFluxoLumin());
			}
		}
		catch  (Exception  e  )  {
			System.out.println(e);
		}  finally  {
			if  (stmt  !=  null)  {  
				stmt.close();  
			}
			if  (con  !=  null)  {  
				con.close();  
			}
		}
	
		return leds;
	}
	
	public Led consultarLed(String cod) throws Exception {
		Led led = new Led();
		Statement  stmt  =  null;
		Connection  con  =  null;
		String  query  =  "select  cod_comp, marca, fluxo_lumin, temp_cor, ind_reprod  from  leds where cod_comp = '"+cod +"'";
		try  {
			con  =  UtilJDBC.getConnection();
			stmt  =  con.createStatement();
			ResultSet  rs  =  stmt.executeQuery(query);
			while  (rs.next())  {
				led.setCodComponente(rs.getString("cod_comp"));  
				led.setMarca(rs.getString("marca"));
				led.setFluxoLumin(rs.getString("fluxo_lumin"));
				led.setTempCor(rs.getString("temp_cor"));
				led.setIndReprod(rs.getString("ind_reprod"));  
			}
		}
		catch  (Exception  e  )  {
			System.out.println(e);
		}  finally  {
			if  (stmt  !=  null)  {  
				stmt.close();  
			}
			if  (con  !=  null)  {  
				con.close();  
			}
		}
	
		return led;
	}
	*/
	public ArrayList<Led> consultarLeds() throws Exception {
		ArrayList<Led> leds = new ArrayList<Led>();
		Led led;
		Statement  stmt  =  null;
		Connection  con  =  null;
		String  query  =  "select  codigoLed, marcaLed, fluxoLuminoso, temperaturaCor, indiceReprod  from  leds";
		try  {
			con  =  UtilJDBC.getConnection();
			stmt  =  con.createStatement();
			ResultSet  rs  =  stmt.executeQuery(query);
			while  (rs.next())  {
				led  =  new  Led();
				led.setCodigoLed(rs.getString("codigoLed"));  
				led.setMarcaLed(rs.getString("marcaLed"));
				led.setFluxoLuminoso(rs.getDouble("fluxoLuminoso"));
				led.setTemperaturaCor(rs.getDouble("temperaturaCor"));
				led.setIndiceReprod(rs.getDouble("indiceReprod"));
				leds.add(led);  
			}
		}  catch  (Exception  e  )  {
			System.out.println(e);
		}  finally  {
			if  (stmt  !=  null)  {  
				stmt.close();  
			}
			if  (con  !=  null)  {  
				con.close();  
			}
		}
		return leds;
	}
	
	public boolean  inserirLed(Led  led)  throws  Exception  {
		boolean retorno  =  false;
		PreparedStatement  stmt  =  null;
		Connection  con  =  null;
		String  query  =  "insert into leds"
				+"(codigoLed, marcaLed, fluxoLuminoso, temperaturaCor, indiceReprod, v0Led, v1Led, v2Led, v3Led, d0Led, d1Led, d2Led, d3Led, ledKe, phi0, ledKv, ledT0, ledRjc, vidaUtil)"
				+ " values  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try  {
			con  =  UtilJDBC.getConnection();
			stmt  =  con.prepareStatement(query);
			stmt.setString(1,  led.getCodigoLed());
			stmt.setString(2,  led.getMarcaLed());  
			stmt.setDouble(3,  led.getFluxoLuminoso());
			stmt.setDouble(4,  led.getTemperaturaCor());
			stmt.setDouble(5,  led.getIndiceReprod());
			stmt.setDouble(6,  led.getV0Led());
			stmt.setDouble(7,  led.getV1Led());
			stmt.setDouble(8,  led.getV2Led());
			stmt.setDouble(9,  led.getV3Led());
			stmt.setDouble(10,  led.getD0Led());
			stmt.setDouble(11,  led.getD1Led());
			stmt.setDouble(12,  led.getD2Led());
			stmt.setDouble(13,  led.getD3Led());
			stmt.setDouble(14,  led.getLedKe());
			stmt.setDouble(15,  led.getPhi0());
			stmt.setDouble(16,  led.getLedKv());
			stmt.setDouble(17,  led.getLedT0());
			stmt.setDouble(18,  led.getLedRjc());
			stmt.setString(19,  led.getVidaUtil());
			
			stmt.executeUpdate();
			retorno  =  true;
		}  catch  (Exception  e  )  {
			System.out.println(e);
		}  finally  {
			if  (stmt  !=  null)  {  
				stmt.close();  
			}  
			if  (con  !=  null)  {  
				con.close();
			}
		}  
		return retorno;  
	}
	
}